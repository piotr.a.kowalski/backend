<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221124000001 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create Child table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE child (id INT AUTO_INCREMENT NOT NULL, room_id INT NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, archived TINYINT(1) NOT NULL, INDEX IDX_22B3542954177093 (room_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE child ADD CONSTRAINT FK_22B3542954177093 FOREIGN KEY (room_id) REFERENCES room (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE child DROP FOREIGN KEY FK_22B3542954177093');
        $this->addSql('DROP TABLE child');
    }
}
