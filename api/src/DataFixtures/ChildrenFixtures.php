<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Child;
use App\Entity\Room;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ChildrenFixtures extends Fixture implements DependentFixtureInterface
{
    private const CHILDREN = [
        [
            'name' => 'Bob',
            'surname' => 'Dylan',
            'archived' => false,
            'room' => RoomFixtures::ROOM_FREE,
        ],

        [
            'name' => 'Matt',
            'surname' => 'Brown',
            'archived' => false,
            'room' => RoomFixtures::ROOM_FULL,
        ],
        [
            'name' => 'John',
            'surname' => 'Smith',
            'archived' => true,
            'room' => RoomFixtures::ROOM_FULL,
        ],
        [
            'name' => 'Rob',
            'surname' => 'Smith',
            'archived' => false,
            'room' => RoomFixtures::ROOM_FULL,
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::CHILDREN as ['name' => $name, 'surname' => $surname, 'archived' => $archived, 'room' => $room]) {
            /** @var Room $roomEntity */
            $roomEntity = $this->getReference($room);

            $child = new Child();
            $child->setName($name);
            $child->setSurname($surname);
            $child->setArchived($archived);
            $child->setRoom($roomEntity);

            $manager->persist($child);
        }

        $manager->flush();

        // simulate removed entity form DB due to behat tests
        $toRemove = $manager->getRepository(Child::class)->find(['id' => 3]);

        if (null !== $toRemove) {
            $manager->remove($toRemove);
            $manager->flush();
        }
    }

    public function getDependencies(): array
    {
        return [
            RoomFixtures::class,
        ];
    }
}
