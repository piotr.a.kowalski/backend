<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Room;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoomFixtures extends Fixture
{
    public const ROOM_FREE = 'room_free';
    public const ROOM_FULL = 'room_full';
    public const ROOM_EMPTY = 'room_empty';

    private const CHILDREN = [
        [
            'name' => 'Apple',
            'capacity' => 10,
            'reference' => self::ROOM_FREE,
        ],
        [
            'name' => 'Banana',
            'capacity' => 2,
            'reference' => self::ROOM_FULL,
        ],
        [
            'name' => 'Watermelon',
            'capacity' => 0,
            'reference' => self::ROOM_EMPTY,
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::CHILDREN as ['name' => $name, 'capacity' => $capacity, 'reference' => $reference]) {
            $room = new Room();
            $room->setName($name);
            $room->setMaxCapacity($capacity);
            $manager->persist($room);

            $this->addReference($reference, $room);
        }

        $manager->flush();
    }
}
