<?php

declare(strict_types=1);

namespace App\Doctrine\Orm\Extension;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Entity\Room;
use Doctrine\ORM\QueryBuilder;

class RoomStatisticsExtension implements QueryCollectionExtensionInterface
{
    private const OPERATION_NAME = '_api_/rooms/statistics_get_collection';

    /** @param array<string, mixed> $context */
    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        Operation $operation = null,
        array $context = []
    ): void {
        $operationName = $context['operation_name'] ?? null;
        if (Room::class !== $resourceClass || self::OPERATION_NAME !== $operationName) {
            return;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];

        $queryBuilder
            ->leftJoin("{$rootAlias}.children", 'children')
            ->select("{$rootAlias} AS room", 'COUNT(children.id) AS count')
            ->groupBy("{$rootAlias}.id");
    }
}
