<?php

declare(strict_types=1);

namespace App\State;

use App\Entity\Room;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class RoomStatisticsDto
{
    #[Groups(['read:statistics'])]
    public int $count = 0;

    #[Groups(['read:statistics'])]
    #[Assert\Valid]
    public Room $room;

    public function __construct(Room $room, int $count)
    {
        $this->room = $room;
        $this->count = $count;
    }
}
