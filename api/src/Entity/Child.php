<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Serializer\Filter\GroupFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ApiResource(
    operations: [
        new Post(uriTemplate: '/children'),
        new Get(
            uriTemplate: '/children/{id}',
            requirements: ['id' => '\d+']
        ),
        new GetCollection(),
    ],
    normalizationContext: [
        'groups' => ['read:child', 'read:child.room'],
    ],
    denormalizationContext: [
        'groups' => 'write:child',
    ]
)]
#[ApiFilter(BooleanFilter::class, properties: ['archived'])]
#[ApiFilter(SearchFilter::class, properties: ['room' => 'exact'])]
#[ApiFilter(GroupFilter::class, arguments: [
    'parameterName' => 'groups',
    'overrideDefaultGroups' => false,
    'whitelist' => ['read:child.room', 'read:room'],
])]
class Child
{
    #[Groups(['read:child'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Assert\NotBlank]
    #[Groups(['read:child', 'write:child'])]
    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[Assert\NotBlank]
    #[Groups(['read:child', 'write:child'])]
    #[ORM\Column(length: 255)]
    private ?string $surname = null;

    #[Groups(['read:child', 'write:child'])]
    #[ORM\Column]
    private bool $archived = false;

    #[Assert\Valid]
    #[Assert\NotBlank]
    #[Groups(['read:child.room', 'write:child'])]
    #[ApiProperty(writableLink: true)]
    #[ORM\ManyToOne(inversedBy: 'children')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Room $room = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function isArchived(): bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(Room $room): self
    {
        $this->room = $room;

        return $this;
    }
}
