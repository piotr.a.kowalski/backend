<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\State\RoomStatisticsDto;
use App\State\RoomStatisticsProvider;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ApiResource(
    operations: [
        new Get(
            uriTemplate: '/room/{id}',
            requirements: ['id' => '\d+']
        ),
        new GetCollection(
            uriTemplate: '/rooms/statistics',
            normalizationContext: [
                'groups' => 'read:statistics',
            ],
            output: RoomStatisticsDto::class,
            provider: RoomStatisticsProvider::class,
        ),
    ],
    normalizationContext: [
        'groups' => 'read:room',
    ],
    denormalizationContext: [
        'groups' => 'write:room',
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact'])]
class Room
{
    #[Groups(['read:room', 'read:child.room', 'read:statistics'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['read:room'])]
    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[Groups(['read:room'])]
    #[ORM\Column]
    private ?int $maxCapacity = null;

    /** @var Collection<int, Child> */
    #[ORM\OneToMany(mappedBy: 'room', targetEntity: Child::class)]
    private Collection $children;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMaxCapacity(): ?int
    {
        return $this->maxCapacity;
    }

    public function setMaxCapacity(int $maxCapacity): self
    {
        $this->maxCapacity = $maxCapacity;

        return $this;
    }

    /**
     * @return Collection<int, Child>
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(Child $children): self
    {
        if (!$this->children->contains($children)) {
            $this->children->add($children);
            $children->setRoom($this);
        }

        return $this;
    }

    public function removeChild(Child $child): self
    {
        $this->children->removeElement($child);

        return $this;
    }

    #[Assert\IsTrue(message: 'There is no more space in this room.')]
    public function isFreeSpace(): bool
    {
        return $this->getChildren()->count() < $this->maxCapacity;
    }
}
