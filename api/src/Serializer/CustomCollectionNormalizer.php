<?php

declare(strict_types=1);

namespace App\Serializer;

use ApiPlatform\Doctrine\Orm\Paginator;
use App\Entity\Child;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CustomCollectionNormalizer implements ContextAwareNormalizerInterface
{
    public const FORMAT = 'json';

    public function __construct(private NormalizerInterface $itemNormalizer)
    {
    }

    /**
     * @param Paginator            $object
     * @param array<string, mixed> $context
     *
     * @return array<string, mixed>
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        $data = [];
        foreach ($object as $item) {
            $data[] = $this->itemNormalizer->normalize($item, $format, $context);
        }

        return [
            'meta' => [
                'start' => $object->getCurrentPage(),
                'limit' => $object->getItemsPerPage(),
                'page_results' => $object->count(),
                'total_results' => $object->getTotalItems(),
            ],
            'data' => $data,
        ];
    }

    /** @param array<string, mixed> $context */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return
            self::FORMAT === $format &&
            $data instanceof Paginator &&
            isset($context['resource_class']) &&
            Child::class === $context['resource_class'];
    }
}
