<?php

declare(strict_types=1);

namespace App\Serializer;

use App\Entity\Child;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CustomItemNormalizer implements ContextAwareNormalizerInterface
{
    public const FORMAT = 'json';

    public function __construct(private NormalizerInterface $itemNormalizer)
    {
    }

    /**
     * @param array<string, mixed> $context
     *
     * @return array<string, mixed>
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        return [
            'data' => $this->itemNormalizer->normalize($object, $format, $context),
        ];
    }

    /** @param array<string, mixed> $context */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return self::FORMAT === $format && $data instanceof Child;
    }
}
