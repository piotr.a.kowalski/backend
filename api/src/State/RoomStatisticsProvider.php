<?php

declare(strict_types=1);

namespace App\State;

use ApiPlatform\Doctrine\Orm\State\CollectionProvider;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Room;

class RoomStatisticsProvider implements ProviderInterface
{
    public function __construct(
        private CollectionProvider $doctrineCollectionProvider
    ) {
    }

    /**
     * @param array<string, mixed> $uriVariables
     * @param array<string, mixed> $context
     *
     * @return array<string, mixed>
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
    {
        /** @var array<string, mixed> $result */
        $result = $this->doctrineCollectionProvider->provide($operation, $uriVariables, $context);
        $statistics = [];

        foreach ($result as ['room' => $room, 'count' => $count]) {
            if ($room instanceof Room && is_int($count)) {
                $statistics[] = new RoomStatisticsDto($room, $count);
            }
        }

        return [
            'data' => $statistics,
        ];
    }
}
